# pacotes básicos
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# classe para inserir valores em dados faltantes
from sklearn.impute import SimpleImputer

# classe para codificar dados categóricos em vetores "OneHot"
from sklearn.preprocessing import OneHotEncoder

"""classe para normalizar dados numéricos que apresentam uma faixa valores 
muito pequena ou muito grande"""
from sklearn.preprocessing import StandardScaler

# classe para realizar as predições
from sklearn.linear_model import LinearRegression

df = pd.read_csv("housing.csv")

print(f"Total de dados faltantes: {df['longitude'].count() - df['total_bedrooms'].count()}")

