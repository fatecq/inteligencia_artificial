from sklearn.datasets import fetch_california_housing

CH = fetch_california_housing(return_X_y=True, as_frame=True)

print(fetch_california_housing()['DESCR'])

print(CH[1])

from sklearn.model_selection import train_test_split

X = CH[0]
y = CH[1]

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25, random_state=6259)

len(X_train)
len(X_test)

print(X_train.describe())

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

df_train = pd.concat([X_train, y_train], axis=1)
graph = sns.histplot(data=df_train, x="MedInc")

graph.axvline(df_train['MedInc'].mean(), 0, y_train.max(), color='green')
# plt.vlines(df_train['MedInc'].mean(), 0, y_train.max(), colors='green')
# plt.show()

graph = sns.histplot(data=df_train, x="Population")

graph.axvline(df_train['Population'].mean(), 0, y_train.max(), color='orange')
# plt.vlines(df_train['Population'].mean(), 0, y_train.max(), colors='orange')
# plt.show()

plt.clf()

plt.scatter(x=df_train['MedInc'], y=df_train['MedHouseVal'], s=1, alpha=0.2)
# plt.show()

from sklearn.linear_model import LinearRegression

modelo_RL = LinearRegression()

modelo_RL.fit(df_train[['MedInc']], y_train)

plt.scatter(df_train[['MedInc']], y_train, s=1, color='purple', alpha=0.2)
A = modelo_RL.coef_
B = modelo_RL.intercept_

plt.plot(df_train[['MedInc']], A * df_train[['MedInc']] + B, color='yellow')
# plt.show()

df_test = pd.concat([X_test, y_test], axis=1)
modelo_RL2 = LinearRegression()
modelo_RL2.fit(df_test[['MedInc']], y_test)

print(modelo_RL.score(df_train[['MedInc']], y_train))
print(modelo_RL2.score(df_test[['MedInc']], y_test))

import numpy as np

train = [0, 0, 0, 0, 0, 0]

train[0] = np.array(X_train[['MedInc']])
train[1] = np.array(X_train[['HouseAge']])
train[2] = np.array(X_train[['AveRooms']])
train[3] = np.array(X_train[['AveBedrms']])
train[4] = np.array(X_train[['Population']])
train[5] = np.array(X_train[['AveOccup']])

m_train = []

for i in range(5):
    m_train.append(LinearRegression())
    m_train[i].fit(train[i], y_train)

for i in range(5):
    print(m_train[i].score(train[i], y_train) * y_train.max())


test = [0, 0, 0, 0, 0, 0]

test[0] = np.array(X_test[['MedInc']])
test[1] = np.array(X_test[['HouseAge']])
test[2] = np.array(X_test[['AveRooms']])
test[3] = np.array(X_test[['AveBedrms']])
test[4] = np.array(X_test[['Population']])
test[5] = np.array(X_test[['AveOccup']])

m_test = []

for i in range(5):
    m_test.append(LinearRegression())
    m_test[i].fit(test[i], y_test)

for i in range(5):
    print(m_test[i].score(test[i], y_test) * y_test.max())