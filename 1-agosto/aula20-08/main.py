# A  tabela  a  seguir  resume  alguns  dados  de  idade  da  pessoa  e  sua  expectativa  de  vida, concluídos a partir
# de uma pesquisa realizada com certa comunidade. Pedem-se:
# a) O coeficiente de correlação linear e equação da regressão linear;
# b) O diagrama de dispersão e o gráfico da equação de regressão linear.
# c) Qual a expectativa de vida de uma pessoa de 85 anos?

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression

idades = np.array([20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70])
expec = np.array([51, 50, 45, 44, 28, 27, 23, 18, 9, 8, 10])

print(len(idades), ',', len(expec))

# Estrutura de dados dicionário
dicionario = {'idade': idades, 'expec': expec}

dfExpec = pd.DataFrame(dicionario)
print(dfExpec)

# Gráfico com o pandas
dfExpec.plot.scatter('idade', 'expec')
# plt.show()

# instanciação
regressor = LinearRegression()

# dados de entrada
X_train = np.array(dfExpec['idade']).reshape(-1, 1)

# dados de saída
y_train = np.array(dfExpec['expec'])

# o treinamento do regressor é feito com o método fit
regressor.fit(X_train, y_train)

# #### Parâmetros do modelo: ####
# modelo: a X + b
# a: coeficiente angular que indica a inclinação
# b: coeficiente linear que indica onde intercepta o eixo

a = regressor.coef_
print(a)

b = regressor.intercept_
print(b)

print(regressor.predict([[20], [22], [60]]))

# ### Interpretação dos parâmetros a e b ###
# b: expectativa de vida e um recém nascido
# a: taxa de decaimento da expectativa de vida por ano vivido
# O modelo de regressão linear é um dos mais simples, por isso:
# - sua acurácia é mais baixa
# - os parâmetros são fáceis de interpretar

plt.scatter(dfExpec['idade'], dfExpec['expec'])
x = np.linspace(dfExpec['idade'].min(), dfExpec['idade'].max(), 100)
y = a * x + b

plt.plot(x, y, '-r', label='y=ax+b')
# plt.show()

# ### Performance de acerto da regressão: R² (coeficiente de determinação) ###
# Pode assumir valores negativos (o que significa que o modelo é pior que a média)
# Se R² for igual a zero ele é tão bom quanto a média
# Se R² for maior que zero ele acerta mais que a média
# Se R² for 1 (máximo) ele acertou todos os dados

print(regressor.score(X_train, y_train))
