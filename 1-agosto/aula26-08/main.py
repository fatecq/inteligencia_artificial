# Método de regressão logística

from sklearn.datasets import load_breast_cancer
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split

# Carregar os dados
dados = load_breast_cancer(as_frame=True)
# print(type(dados))
# print(dados.DESCR)

print('\nDados de entrada')
print(dados.data)

print('\nSaída')
print(dados.target)
# print(type(dados.target))

print('\nDistribuição da classe:'
      '\n0 - benigno'
      '\n1 - maligno'
      '\n--------------')
print(dados.target.value_counts())

X = dados.data
y = dados.target

# Instanciar a classe
# classificador = LogisticRegression()

# Treinamento
# classificador.fit(X, y)

# Trienamento 2
classificador_2 = LogisticRegression(max_iter=5000)
classificador_2.fit(X, y)

print('\nScore')
print(classificador_2.score(X, y))

# Dividindo os dados em duas partes: dados de treinamento e dados de teste
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Retreinando
classificador_3 = LogisticRegression(max_iter=5000)
classificador_3.fit(X_train, y_train)

print('\nScore 2')
print(classificador_3.score(X_train, y_train))

print('\nScore 3')
print(classificador_3.score(X_test, y_test))
