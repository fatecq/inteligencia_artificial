# A seguir, estão registrados os níveis de ruído (em decibéis), de algumas áreas resistenciais
# de uma cidade. Construa uma tabela de distribuição de frequência de classes completa.
#
# 65, 73, 59, 73, 64, 73, 66, 64, 65, 66, 72, 68, 67, 65, 59, 58, 65, 60, 70, 70, 68, 58
# 70, 56, 63, 72, 58, 59, 59, 64, 70, 64

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

decibeis = np.array(
    [65, 73, 59, 73, 64, 73, 66, 64, 65, 66, 62, 68, 67, 65, 59, 58, 65, 60, 70, 70, 68, 58, 70, 56, 63, 72, 58, 59, 59,
     64, 70, 64])

print('\n1 linha e 32 colunas')
print(decibeis)
print(decibeis.shape)  # mostra a forma do array

decibeis2 = np.array([
    [65, 73, 59, 73, 64, 73, 66, 64, 65, 66, 62, 68, 67, 65, 59, 58],
    [65, 60, 70, 70, 68, 58, 70, 56, 63, 72, 58, 59, 59,
     64, 70, 64]])

print('\n2 linhas e 16 colunas')
print(decibeis2)
print(decibeis2.shape)

print('\n2 linhas e 16 colunas')
decibeis2l = decibeis.reshape(2, -1)  # reformata o array de acordo com o valor especificado
print(decibeis2l)
print(decibeis2l.shape)

print('\n32 linhas e 1 colunas')
decibeis1c = decibeis.reshape(1, -1)
print(decibeis1c)
print(decibeis1c.shape)

# Utilizando DataFrames
dfDecibeis = pd.DataFrame(decibeis, columns=['Decibéis'])
print('\nPandas DataFrame')
print(dfDecibeis)

print('\nValores únicos')
print(np.unique(decibeis))

print('\nFrequência dos valores')
print(dfDecibeis['Decibéis'].value_counts())

print('\nGráfico')
print(plt.hist(decibeis, bins=6, ec='black'))  # exibe um gráfico a partir do array
plt.show()

print('\nMédia')
media = decibeis.mean()
print(media)

print('\n')
print(plt.axvline(x=media, color='red'))
plt.show()


