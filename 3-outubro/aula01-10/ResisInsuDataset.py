import numpy as np
import pandas as pd


def logistic(x_ins, y_gli, k=5., x_0=2.7):
    """função logistica com growth rate de 5.0 (k=5.) e midle point de 2.7 (x_0=2.7)"""
    x = (x_ins * y_gli) / 405.
    return 1.0 / (1.0 + np.exp(-k * (x - x_0)))


def DataSet(len_data=2000, insu_range=(0, 20), glico_range=(50, 200)):
    """retorna len_data pacientes com o par insulina e glicose e classifica se o 
    paciente apresenta resistencia a insulina"""

    insulina = np.random.uniform(insu_range[0], insu_range[1], len_data)
    glicose = np.random.uniform(glico_range[0], glico_range[1], len_data)
    classe = np.random.binomial(n=1, p=logistic(insulina, glicose))
    d = {'insu': insulina, 'glic': glicose, 'class': classe}
    return pd.DataFrame(data=d)
