"""
## Classificação com Árvores de Decisão

A AD que classifica apresenta uma estratégia parecida com a AD de Regressão. Ela realiza divisões do espaço das features
minimizando o erro da classificação. Para isso ela usa medidas de impureza como Gini e Entropia.

O objetivo é que a árvore tenha folhas com o mínimo de impureza possível.
O que é Impureza?
Uma gaveta com camisas e bermudas misturadas é uma gaveta impura. Já uma gaveta que tem apenas camisas de um lado e de
outro lado apenas bermudas, pode ser divida em duas parte com alto nível de pureza (ou nível de impureza zero).

As árvores classificadoras vão procurar fazer o mesmo. Elas vão procurar as melhores divisões no espaço das variáveis de
entrada de tal forma que minimize a impureza das classes (mistura das classes) em cada uma das folhas.

Neste exercício, voces irão mostrar que a medida em que a arvore cresce (com o surgimento das folhas), a impureza vai se
tornando cada vez menor.
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

"""
## Carregando e Analisando os dados utilizados

os dados utilizados serão dados fictícios sobre pacientes que podem apresentar resistência insulínica que é um problema 
de saúde relacionado a diabetes e a outros problemas de saúde. Para identificar se um paciente tem ou não esta 
complicação metabólica é usado a medida de glicose e de insulina no sangue. Dependendo dos níveis de glicose e insulina 
o paciente pode ou não ter este problema metabólico. 

Em geral índices baixos de insulina (e glicose média ou baixa), significa que o paciente apresenta boa saude. 

Vamos usar as ADs para criar um modelo que possa predizer se um paciente tem ou não resistencia insulinica.

Os dados foram construidos de acordo com o **índice de HOMA**, um critério médico frequentemente utilizado para 
determinar se um paciente tem ou não resistência à insulina.

Os dados podem ser baixados pelo método DataSet() do módulo ResisInsuDataset. Para tanto é necessário ter o 
arquivo ResisInsuDataset.py 
"""

from ResisInsuDataset import DataSet

df = DataSet()

"""
# visualização dos dados

Use a função scatterplot(), do seaborn,  para fazer a vizualização dos dados. Faça o gráfico com os dados de todos os 
pacientes para entender a relação da resistência insulínica com as variáveis de entrada (features) glicose e insulina.
"""

sns.scatterplot(data=df, x='insu', y='glic', hue='class', alpha=0.3)
plt.show()

# Fase de treinamento
# a partir do módulo sklearn.tree, importe a classe DecisionTreeClassifier

from sklearn.tree import DecisionTreeClassifier

# separe do DataFrame os dados de entrada (colunas insulina e a glicose) e os dados de saída (coluna com o diagnóstico)
X = df[['insu', 'glic']]
y = df['class']

# instancie a classe DecisionTreeClassifier passando max_depth=2 , na sequencia treine o modelo
modelo = DecisionTreeClassifier(max_depth=2)

modelo.fit(X, y)

# faça o plot da árvore com a função plot_tree do módulo sklearn.tree
from sklearn.tree import plot_tree

plt.clf()  # limpar o gráfico
plot_tree(modelo)
plt.show()
