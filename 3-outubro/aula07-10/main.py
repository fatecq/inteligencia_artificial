# Modelos Lineares de Classificação

# vamos revisitar o problema de diagnóstico de cancer de mama. vamos seguir o seguinte roteiro:

# * [1] Entender os dados de entrada

# * [2] Visualizar os dados de entrada

# * [3] Normalizar e Balancear os dados de entrada

# * [4] Analisar as visualizações

# * [5] Utilizar 3 modelos lineares de Classificação

# * [6] Comparar os 3 modelos e a rede neural de um neurônio

# importando as classes dos algoritmos
from sklearn.linear_model import LogisticRegression
from sklearn.linear_model import SGDClassifier
from sklearn.linear_model import Perceptron

# importando a classe com os dados de Câncer de mama
from sklearn.datasets import load_breast_cancer

# classe para normalizar os dados
from sklearn.preprocessing import StandardScaler

# bibliotecas básicas para manipular e visualizar os dados
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

import shutup  # esconder os warnings

shutup.please()  # ignorando os warnings em tempo de execução

# carregando os dados
dados = load_breast_cancer(return_X_y=True, as_frame=True)

# dados de saída (o diagnóstico) 357 benignos e 212 malignos
print(dados[1].value_counts())

# selecionar as 3 features (Worst Area, Worst Smoothness and Mean Texture)
X = dados[0][['worst area', 'worst smoothness', 'mean texture']]

y = dados[1]

df = pd.concat([X, y], axis=1)

'''
### Fazendo um conjunto de gráficos em uma única saída

Há várias formas de implementar isso. Pode-se usar a função ``` plt.subplots()``` como o trecho de código abaixo:

    fig, ax = plt.subplots(<n_linhas>,<n_colunas>)

onde <n_linhas> e <n_colunas> são inteiros para indicar a quantidade de linhas e colunas do conjunto de gráficos.
'''

fig, axes = plt.subplots(1, 2, figsize=(10, 4))

sns.histplot(data=df, x='worst area', ax=axes[0])
sns.histplot(data=df, x='worst area', hue='target', ax=axes[1])
# plt.show()

# como saber o maior e o menor valor da feature worst area?
print(f"\nMaior valor: {df['worst area'].max()}")
print(f"Menor valor: {df['worst area'].min()}")

# como saber o maior e menor valor da feature worst area para os casos malignos e benignos?
print(f"\nMaior valor maligno: {df['worst area'][df['target'] == 0].max()}")
print(f"Menor valor maligno: {df['worst area'][df['target'] == 0].min()}")

print(f"\nMaior valor benigno: {df['worst area'][df['target'] == 1].max()}")
print(f"Menor valor benigno: {df['worst area'][df['target'] == 1].min()}\n")

# Normalizar os dados de entrada (evitar números grandes)
print(X.describe())

scaler = StandardScaler()
scaler.fit(X)

X_scaler = scaler.transform(X)
X_scaler = pd.DataFrame(X_scaler)

X_scaler.columns = X.columns
print(X_scaler)

df_scaler = pd.concat([X_scaler, y], axis=1)

# os mesmo gráficos com os valores normalizados
fig, axes = plt.subplots(1, 2, figsize=(10, 4))

sns.histplot(data=df_scaler, x='worst area', ax=axes[0])
sns.histplot(data=df_scaler, x='worst area', hue='target', ax=axes[1])
# plt.show()

# Comparação entre os modelos lineares de classificação
# modelos com dos dados não normalizados
modeloLR = LogisticRegression()
modeloSGD = SGDClassifier()
modeloPCT = Perceptron()

modeloLR.fit(X, y)
modeloSGD.fit(X, y)
modeloPCT.fit(X, y)

print('\n### Dados não normalizados ###')
print(f"LogisticRegression: {modeloLR.score(X, y).round(2) * 100}%")
print(f"SGDClassifier: {modeloSGD.score(X, y).round(2) * 100}%")
print(f"Perceptron: {modeloPCT.score(X, y).round(2) * 100}%")

# modelos com dos dados normalizados
modeloLR1 = LogisticRegression()
modeloSGD1 = SGDClassifier()
modeloPCT1 = Perceptron()

modeloLR1.fit(X_scaler, y)
modeloSGD1.fit(X_scaler, y)
modeloPCT1.fit(X_scaler, y)

print('\n### Dados normalizados ###')
print(f"LogisticRegression: {modeloLR1.score(X_scaler, y).round(2) * 100}%")
print(f"SGDClassifier: {modeloSGD1.score(X_scaler, y).round(2) * 100}%")
print(f"Perceptron: {modeloPCT1.score(X_scaler, y).round(2) * 100}%")

# compartilhando aprendizado entre os modelos
modeloPCT1.coef_ = modeloLR1.coef_
modeloPCT1.intercept_ = modeloLR1.intercept_

print(modeloPCT1.score(X_scaler, y).round(2) * 100)

# mostrando os métodos modeloLR1.predict(), modeloLR1.predict_proba()
entrada = np.array(X_scaler.iloc[3]).reshape(1, -1)

print(f"\nMedição 1: {modeloLR1.predict(entrada)}")
print(f"\nMedição 2 (probabilidade): {modeloLR1.predict_proba(entrada)}")

# ### Escrevendo uma rede neural de um único neurônio ###
pesos = modeloLR1.coef_

bias = modeloLR1.intercept_

# função dot para fazer a multiplicação dos pesos pelas entradas
# (P1*WA + P2*WS + P3*MT) + bias
z = np.dot(pesos, entrada.T) + bias


# criando a função de ativação
def neuronio(x):
    return 1. / (1. + np.exp(-x))


# a função neurônio mostra a probabilidade do diagnóstico ser benigno (1)
print(f'\nProbabilidade do diagnóstico ser benigno: {neuronio(z)}')

# mostrando a probabilidade do diagnóstico ser maligno (0)
print(f'Probabilidade do diagnóstico ser maligno: {(1 - neuronio(z))}')


def proba(x):
    return np.array([(1 - neuronio(x)), neuronio(x)])


print(proba(z), modeloLR1.predict_proba(entrada))


# criando a rede neural recebendo os dados de todos os pacientes
def neuronio1(x, p, b):
    z = np.dot(p, x.T) + b
    prob = 1. / (1. + np.exp(-z))
    return prob


print(f'\n Listando as probablilidades de todos os pacientes: \n{neuronio1(X_scaler, pesos, bias)}')

# ### Construindo uma rede neural com muitos neurônios ###
from sklearn.neural_network import MLPClassifier

# instanciando a classe
modeloNN = MLPClassifier(max_iter=1000)
modeloNN1 = MLPClassifier(hidden_layer_sizes=(30,), activation='logistic', solver='sgd', max_iter=1000)

modeloNN.fit(X_scaler, y)
modeloNN1.fit(X_scaler, y)

print(modeloNN.score(X_scaler, y), modeloNN1.score(X_scaler, y))
