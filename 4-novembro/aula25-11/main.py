import pandas as pd
import matplotlib.pyplot as plt
from sklearn.preprocessing import OneHotEncoder

df = pd.read_csv("housing.csv")

# instanciando a classe
encoder = OneHotEncoder()

# criar a codificação OneHotEncoder
OP_cat = encoder.fit_transform(df[['ocean_proximity']])

# colocar a codificação OneHotEncoder no DataFrame
df[['<1H OCEAN', 'INLAND', 'ISLAND', 'NEAR BAY', 'NEAR OCEAN']] = OP_cat.toarray()

# ## Comparando a performance do algoritmo LinearRegression com os dados, levando em conta os valores categóricos
# codificados ###

# fazendo o modelo BaseLine
from sklearn.linear_model import LinearRegression

modelo_BL = LinearRegression()

X = df[['median_income']]
y = df[['median_house_value']]

modelo_BL.fit(X, y)

print(f'\nPerformance do modelo linear: {modelo_BL.score(X, y)}')

# fazendo o modelo incluindo a codificação dos dados categóricos
modelo_cat = LinearRegression()

X_cat = df[['median_income', '<1H OCEAN', 'INLAND', 'ISLAND', 'NEAR BAY', 'NEAR OCEAN']]

modelo_cat.fit(X_cat, y)

print(f'Performance do modelo linear categórico: {modelo_cat.score(X_cat, y)}\n')

# ### Visualizando os dados ###
plt.scatter(x=df['median_income'], y=df['median_house_value'], s=1, alpha=0.2)
plt.show()

# removendo o valor máximo das casas (teto)
max = df['median_house_value'].max()
df_sem_teto = df[df['median_house_value'] < max]

modelo_ST_BL = LinearRegression()
modelo_ST_cat = LinearRegression()

X_ST_BL = df_sem_teto[['median_income']]
X_ST_cat = df_sem_teto[['median_income', '<1H OCEAN', 'INLAND', 'ISLAND', 'NEAR BAY', 'NEAR OCEAN']]

y_ST = df_sem_teto['median_house_value']

modelo_ST_BL.fit(X_ST_BL, y_ST)
modelo_ST_cat.fit(X_ST_cat, y_ST)

print(f'\nPerformance do modelo linear sem teto: {modelo_ST_BL.score(X_ST_BL, y_ST)}')
print(f'Performance do modelo linear categórico sem teto: {modelo_ST_cat.score(X_ST_cat, y_ST)}')
