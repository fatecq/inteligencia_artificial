import pandas as pd
import matplotlib.pyplot as plt

df = pd.read_csv("housing.csv")

print(df.info())

print(df.describe())

print(df['ocean_proximity'].value_counts())

# ### Fazer uma divisão representativa de dados de treinamento e testes ###
# 1. importando a classe que divide os dados
from sklearn.model_selection import StratifiedShuffleSplit

# 2. usar a renda média como critério de representatividade
import numpy as np

df['income_cat'] = pd.cut(df['median_income'],
                          bins=[0.0, 1.5, 3.0, 4.5, 6.0, np.inf],
                          labels=[1, 2, 3, 4, 5])

# 3. instanciando a classe que divide os dados representativamente
stratifier = StratifiedShuffleSplit(n_splits=1, test_size=0.2)

for ind_train, ind_test in stratifier.split(df, df['income_cat']):
    df_train = df.loc[ind_train]
    df_test = df.loc[ind_test]

# fazendo um mapa dos dados
df.plot(kind='scatter', x='longitude', y='latitude',
        s=df['population'] / 100,
        c="median_house_value", cmap=plt.get_cmap("jet"),
        colorbar=True,
        alpha=0.2)
plt.show()

# ### Limpeza de dados ###
from sklearn.impute import SimpleImputer

# 1. instanciar a classe
imputer = SimpleImputer(strategy='median')

df_num = df.drop('ocean_proximity', axis=1)

imputer.fit(df_num)

X = imputer.transform(df_num)  # a saída é uma matriz bidimensional 20640x9

# 2. transformando a matriz em DataFrame
df_num_imp = pd.DataFrame(X)

df_num_imp.columns = df_num.columns
df_num_imp.index = df_num.index

# ### Como trabalhar com dados categóricos ###
from sklearn.preprocessing import OrdinalEncoder

ordinal_encoder = OrdinalEncoder()

df["op_num"] = ordinal_encoder.fit_transform(df[['ocean_proximity']])

# fazendo um mapa dos dados
df.plot(kind='scatter', x='longitude', y='latitude', c='op_num', cmap=plt.get_cmap('jet'), s=1, alpha=0.1)
plt.show()

from sklearn.preprocessing import OneHotEncoder
hot_encoder = OneHotEncoder()

sparse_matrix = hot_encoder.fit_transform(df[['ocean_proximity']])

print(f'\nDados categóricos em forma de matriz:\n'
      f'{sparse_matrix.toarray()}\n')

print(f'Valor dos dados da matriz, de acordo com a posição do número 1:\n'
      f'{hot_encoder.categories_}\n')

df[['<1H OCEAN', 'INLAND', 'ISLAND', 'NEAR BAY', 'NEAR OCEAN']] = sparse_matrix.toarray()

df.drop('ocean_proximity', axis=1, inplace=True)

print(df.T)

# ### Testando o HotEncoder em um algoritmo de machine learning ###
# Comparar a performance do HotEncoder com outra estratégia

# ## Modelo Baseline
# Utilizando apenas a coluna "median_income" para predizer o valor das casas
df.plot(kind='scatter', x='median_income', y='median_house_value', s=1, alpha=0.1)
plt.show()

df['median_house_value'].hist(bins=200)
plt.show()

# remover as casas com valor máximo devido à metodologia da pesquisa
teto = df['median_house_value'].max()
df_sem_teto = df[df['median_house_value'] < teto]

df_sem_teto['median_house_value'].hist(bins=200)
plt.show()
