# importando a classe com os dados de Câncer de mama
from sklearn.datasets import load_breast_cancer

# carregando os dados
dados = load_breast_cancer(return_X_y=True, as_frame=True)

# dados de saída
print(dados[1].value_counts())

print(f'\nPorcentagem dos malignos: {212/(357+212)}')

print(f'Porcentagem dos benignos: {357/(357+212)}\n')

# atribuindo os dados de entrada
X = dados[0]

# atribuindo os dados de saída
y = dados[1]

import pandas as pd

df = pd.concat([X, y], axis=1)

dfM = df[df['target'] == 0]
dfB = df[df['target'] == 1]

dfaux = dfM.sample(n=145, random_state=22)

dfM = pd.concat([dfM, dfaux], axis=0, ignore_index=True)
print(dfM)

# juntar dfM balanceado com dfB para fazer o balanceamento
df_balanceado = pd.concat([dfM, dfB], axis=0, ignore_index=True)

# importar a classe de rede neural
from sklearn.neural_network import MLPClassifier

# instanciando a classe
modeloNN = MLPClassifier(max_iter=1000)
modeloNN1 = MLPClassifier(max_iter=1000, hidden_layer_sizes=(30,), activation='logistic', solver='sgd')

# treinamento
modeloNN.fit(df_balanceado[['mean texture', 'worst area', 'worst smoothness']], df_balanceado['target'])
modeloNN1.fit(df_balanceado[['mean texture', 'worst area', 'worst smoothness']], df_balanceado['target'])

score = modeloNN.score(df_balanceado[['mean texture', 'worst area', 'worst smoothness']], df_balanceado['target'])
print(f'Aproveitamento modeloNN: {score}\n')

score = modeloNN1.score(df_balanceado[['mean texture', 'worst area', 'worst smoothness']], df_balanceado['target'])
print(f'Aproveitamento modeloNN1: {score}')
