"""
### 1. Dados fictícios sobre Mercado Imobiliário
Neste exercicio vamos usar dados construidos de forma fictícia. Trata-se de dados de casas que apresentam como entrada o
metro quadrado construído e a respectiva resposta que é o preço do imóvel. O objetivo do exercício é usar o modelo de
Árvore de Decisão para precificar o mercado imobiliário.

Ao longo do exercício serão usados poucos dados, propositalmente. Isto tenta simular uma situação real em que não temos
acesso a muitos dados que pretendemos analisar.
Siga as instruções para realizar o exercicio:

Instancie a classe DataSet do módulo ImovelDataset e use o método train para obter seus dados. Antes de usar o método
train para gerar seus dados vc deve mudar o atributo seed do referido módulo. O atributo seed é um inteiro e vem como
padrão seed = 0. Mude o valor deste atributo utilizando os 4 primeiros digitos do seu RG. Realizando este procedimento,
o método train vai gerar dados de treinamento específicos para você que serão diferentes do seu colega. Ou seja, cada
aluno terá seus dados próprios de acordo com seu numero de RG.
O método train vai retornar duas coisas: uma variável de entrada (o metro quadrado da casa) e uma variável de saída
(o respectivo preço da casa)
"""
import numpy as np

from ImovelDataset import DataSet

dados = DataSet(seed=6259)

casas = dados.train()

'''
### 2.Checando os dados de treinamento
Verifique os valores que irá usar. Para tanto faça:

    1) Qual é a estrutura de dados utilizada pelo métodos valid e train para armazenar os valores? (utilize a função 
    type() para responder)
        1.1) Qual a estrutura dentro da tupla (encontrada anteriormente)?
    2) Conte quantos exemplos (a quantidade de casas) foram gerados para voce fazer este exercício (use a função len())
    3) Qual o formato dos dados de entrada? (utilize o metodo shape) qual a razão para o formato colocado ?
    4) Faça um gráfico com a função scatter do pyplot e observe a relação entre metro quadrado da casa e seu preço. 
    O que é possivel dizer sobre os preços das casas pelo gráfico?
    5) Use o algoritmo de regressão linear para tentar precificar o mercado imobiliário desta cidade fictícia
'''
# 1)
print(type(casas))

# 1.1)
print(type(casas[0]))

# 2)
print(len(casas[0]))

# 3)
print(casas[0].shape)  # dados de entrada (metro quadrado construído)
print(casas[1].shape)  # dados de saída (preço da casa)

# 4)
import matplotlib.pyplot as plt

plt.scatter(casas[0], casas[1])
# plt.show()

# 5)
from sklearn.linear_model import LinearRegression

modelo_rl = LinearRegression()

X = casas[0]
y = casas[1]

modelo_rl.fit(X, y)

# plotar os dados com a reta de regressão linear
plt.scatter(X, y)

A = modelo_rl.coef_
B = modelo_rl.intercept_

plt.plot(X, A * X + B, color='y')
# plt.show()

# quanto é o metro quadrado construído?
print((modelo_rl.coef_ * 1000).round())

# preço do terreno
print((modelo_rl.intercept_ * 1000).round())

# criando um dataframe com as 20 casas da imobiliária
import pandas as pd

dic_casas = {'metragem': X.reshape(-1), 'preco': y}

df_casas = pd.DataFrame(dic_casas)

# criando uma coluna com os preços do modelo de regressão linear
df_casas['preco_modelo'] = modelo_rl.predict(X)

# criando uma coluna daa diferença entre valor do proprietário e valor de mercado
df_casas['dif'] = df_casas['preco'] - df_casas['preco_modelo']

print(df_casas)

# selecionando as casas abaixo do valor de mercado
df_oportunidade = df_casas[df_casas['dif'] < 0]

print("\nCasas abaixo do valor de mercado: ")
print(df_oportunidade)

# ordenando por ordem das mais atrativas
df_oportunidade = df_oportunidade.sort_values('dif')
print(df_oportunidade)

# qual a perspectiva de lucro?
print("\nPerspectiva de lucro: ")
print(df_oportunidade['dif'].sum())

'''
### 3. Instanciando a classe DecisionTreeRegressor()

Instancie a classe que implementa a árvore de decisão, ou seja, crie o objeto da classe DecisionTreeRegressor() que se 
encontra no módulo sklearn.Tree. Ao criar o objeto sem passar nenhum parametro da classe que a modifique, a árvore 
crescerá o máximo possível. Ela crescerá até que exista apenas um único exemplo em cada folha.
'''
from sklearn.tree import DecisionTreeRegressor, plot_tree

modelo_dtr = DecisionTreeRegressor(max_depth=2)

modelo_dtr.fit(X, y)

# plot_tree(modelo_dtr)
# plt.show()

# explicando as informações nos nós e folhas:
# samples: total de casas
# value: média dos valores

# vamos fazer um modelo de árvore sem passar nenhum parâmetro
modelo_dtr1 = DecisionTreeRegressor()

modelo_dtr1.fit(X, y)

# plot_tree(modelo_dtr1)
# plt.show()

modelo_dtr2 = DecisionTreeRegressor(min_samples_leaf=3)

modelo_dtr2.fit(X, y)

# plot_tree(modelo_dtr2)
# plt.show()

# usando o min_sample_split

modelo_dtr3 = DecisionTreeRegressor(min_samples_split=5)

modelo_dtr3.fit(X, y)

# plot_tree(modelo_dtr3)
# plt.show()

# temos vários modelos de árvore, qual apresenta o melhor desempenho?

dados_valid = dados.valid()

# dos 5 modelos (estudantes), qual irá tirar a melhor nota?

X_valid = dados_valid[0]
y_valid = dados_valid[1]

print(f'\n1: {modelo_rl.score(X_valid, y_valid)}')
print(f'1.1: {modelo_rl.score(X, y)}')

print(f'\n2: {modelo_dtr.score(X_valid, y_valid)}')
print(f'2.1: {modelo_dtr.score(X, y)}')

print(f'\n3: {modelo_dtr2.score(X_valid, y_valid)}')
print(f'3.1: {modelo_dtr2.score(X, y)}')

print(f'\n4: {modelo_dtr3.score(X_valid, y_valid)}')
print(f'4.1: {modelo_dtr3.score(X, y)}')

X_plot_tree = np.linspace(X.min(), X.max(), 200)
y_plot_tree = modelo_dtr.predict(X_plot_tree.reshape(-1, 1))

# dados de treinamento com a predição da árvore
plt.plot(X_plot_tree, y_plot_tree, color='r')
plt.scatter(X, y)
# plt.show()

# dados de validação com as predições
plt.clf()  # limpar o gráfico

X_plot_tree_valid = np.linspace(X_valid.min(), X_valid.max(), 200)
y_plot_tree_valid = modelo_dtr.predict(X_plot_tree_valid.reshape(-1, 1))

plt.plot(X_plot_tree_valid, y_plot_tree_valid, color='orange')
plt.scatter(X_valid, y_valid)
# plt.show()

'''
### 3.1.1 Quantidade de folhas do modelo treinado

Quantas folhas tem sua árvore? Para responder esta pergunta use a função plot_tree() para visualizar a arvore e contar o
numero de nós terminais (as folhas) da sua arvore. Uma forma mais simples de obter o número é usar o atributo 
tree_.n_leaves (ou get_n_leaves) do objeto instanciado, ele retornará um inteiro que informa a quantidade de folhas 
da sua arvore.
Explique porque ela tem esta quantidade de folhas.
'''
