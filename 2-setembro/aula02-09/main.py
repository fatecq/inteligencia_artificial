import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from sklearn.linear_model import LinearRegression

df = pd.read_csv("CO2EmissionsCanada.csv")
dados = df[['Fuel Consumption Comb (L/100 km)', 'CO2 Emissions(g/km)']]
dados.columns = ['consumo', 'emissao']
print(dados)

# ### Regressão linear ###
# instanciando a classe
modelo = LinearRegression()

# fase de treinamento
X = np.array(dados['consumo']).reshape(-1, 1)
y = dados['emissao']
modelo.fit(X, y)

print("\nCoeficiente angular")
print(modelo.coef_)

print(modelo.intercept_)

# gráfico
plt.scatter(dados['consumo'], dados['emissao'], s=1)
# plt.show()

# ### Clusterização ###
# instanciar a classe
modelo_cluster = KMeans(n_clusters=5, random_state=22)

# treinamento para achar os clusters
modelo_cluster.fit(dados)
print(modelo_cluster.labels_)

# inserir os rótulos no dataframe dados
dados.insert(2, 'grupo', modelo_cluster.labels_)

# sns.scatterplot(data=dados, x='consumo', y='emissao', hue='grupo')
# plt.show()

# criando uma nova coluna com a coordenada angular
angulo = np.degrees(np.arctan2(dados['emissao'], dados['consumo']))

# inserindo a variável angulo no dataframe dados
dados.insert(3, 'angulo', angulo)

# gráfico com a coluna angulo
# plt.scatter(dados['angulo'], dados['emissao'], s=3)
# plt.show()

# remover o outlier
dados = dados[dados['angulo'] > 86]

x_angulo = np.array(dados['angulo']).reshape(-1, 1)
modelo_cluster.fit(x_angulo)

grupo_novo = modelo_cluster.labels_

# inserir grupo novo no dataframe
dados.insert(4, 'grupo_novo', grupo_novo)

sns.scatterplot(data=dados, x='consumo', y='emissao', hue='grupo_novo', s=3, palette=sns.color_palette('tab10', 5))
# plt.show()

# separando os carros em clusters (grupos)
df0 = dados[dados['grupo_novo'] == 0]
df1 = dados[dados['grupo_novo'] == 1]
df2 = dados[dados['grupo_novo'] == 2]
df3 = dados[dados['grupo_novo'] == 3]
df4 = dados[dados['grupo_novo'] == 4]

#  ##### Exercício 9 de setembro ####
# ### 1. Crie uma lista com o dataframe de cada grupo, usando laço for: (há, pelo menos, duas formas de fazer isso) ###
df_cluster = [dados[dados['grupo_novo'] == i] for i in range(5)]

# ### 2. Conte quantos carros tem em cada grupo ###
for i in range(5):
    print(len(df_cluster[i]))

# ### 3. Realize a regressão linear de cada um dos grupos e imprima os coeficientes angulares de cada grupo.
# Para tanto, use um laço for e inicie uma lista vazia; dentro do laço, adicione os elementos da lista com os objetos da
# classe LinearRegression ###

lista = []
for i in range(10):
    lista.append(i ** 2)
print(lista)

modelos = []
for i in range(5):
    modelos.append(LinearRegression())
    x_cluster = np.array(df_cluster[i]['consumo']).reshape(-1, 1)
    y_cluster = df_cluster[i]['emissao']
    modelos[i].fit(x_cluster, y_cluster)

for i in range(5):
    print(modelos[i].coef_)

# ### 4. Qual a interpretação para o coeficiente angular do modelo de Regressão Linear? ###
# R: O coeficiente angular determina a taxa de emissão de CO2 de cada grupo de carros. Ou seja, a cada unidade de
# consumo aumentada, a emissão é aumentada pelo valor do coeficiente angular.

# ### 5. Ordene de forma ascendente os grupos (clusters) segundo a taxa de emissão. Indique o respectivo grupo.
# Para tanto, crie um dataframe com uma coluna dos grupos e outra coluna com as taxas de emissão ###
taxa = []

for i in range(5):
    taxa.append(modelos[i].coef_.round(2).item())
print(taxa)

# transformando a lista taxa em um dataframe
df_taxas = pd.DataFrame(taxa)

# dando nome à coluna do dataframe
df_taxas.columns = ['taxas']

df_taxas['grupo'] = [0, 1, 2, 3, 4]

# ordenando a lista de forma persistente
df_taxas.sort_values('taxas', inplace=True)
print(df_taxas)

# ### 6. Os valores da coluna 'grupo_novo' do dataframe dados são apenas um rótulo, ou seja, o valor do inteiro que
# rotula cada grupo não apresenta interpretação. Portanto, vamos adicionar na tabela 'dados', uma coluna com a taxa
# de emissão. Pois esta nova coluna será capaz de dar interpretação aos dados além de servir como rótulo para cada
# grupo. Para realizar esta tarefa, utilize a função pd.merge() ###

dados = pd.merge(dados, df_taxas, left_on='grupo_novo', right_on='grupo')
print(dados)

# ### 6.1 Por que foram criadas as colunas com os nomes grupo_x e grupo_y? ###
# R: Como ambas as estruturas mescladas no comando anterior tinham uma coluna "grupo", o pandas adicionou,
# dinamicamente, uma a mais nome às colunas.

# ### 7. Refaça o gráfico do consumo versus emissão (com sns.scatterplot(), passe  hue = 'taxas')
# usando dataframe dados ###
sns.scatterplot(data=dados, x='consumo', y='emissao', hue='taxas', s=7, palette=sns.color_palette('tab10', 5))
# plt.show()

# ### 8. A identificação de cada grupo de carros pela coluna 'taxa' ajudou em alguma coisa? Justifique sua resposta. ###
# R: Sim

# ### 9. Coloque a reta da regressão linear para cada um dos grupos. Para isso, use a lista modelos para acessar os
# coeficientes angular e linear e construir as retas. Use a função plt.plot() para razer os gráficos ###

# lembrando que o Y do modelo é Y=ax+b
x_plot = np.array(dados['consumo'])

for i in range(5):
    A = modelos[i].coef_
    B = modelos[i].intercept_
    Y_model = A * x_plot + B
    plt.plot(x_plot, Y_model, color='black', linewidth=0.5, alpha=0.5)
# plt.show()
