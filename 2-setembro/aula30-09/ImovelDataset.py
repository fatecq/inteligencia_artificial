#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np


class DataSet:

    def __init__(self, seed, N_train=20, N_valid=5, N_large=500):
        self.seed = seed
        self.N_train = N_train
        self.N_valid = N_valid
        self.N_large = N_large
        self.A = np.random.RandomState(self.seed).uniform(1.5, 3.0)
        self.B = np.random.RandomState(self.seed).randint(100, 300)

    def train(self):
        X = np.random.RandomState(self.seed + 1).uniform(30, 300, (self.N_train, 1))
        y = self.A * X.reshape(-1) + self.B + 70 * np.random.RandomState(self.seed + 1).randn(self.N_train)
        return X, y

    def valid(self):
        X = np.random.RandomState(2 * self.seed).uniform(30, 300, (self.N_valid, 1))
        y = self.A * X.reshape(-1) + self.B + 70 * np.random.RandomState(2 * self.seed).randn(self.N_valid)
        return X, y

    def train_large(self):
        X = np.random.RandomState(self.seed + 1).uniform(30, 300, (self.N_large, 1))
        y = self.A * X.reshape(-1) + self.B + 70 * np.random.RandomState(self.seed + 1).randn(self.N_large)
        return X, y

    def train_3(self):
        X_1 = np.random.RandomState(self.seed + 2).uniform(30, 300, (self.N_train, 1))
        X_2 = np.random.RandomState(self.seed + 3).uniform(30, 300, (self.N_train, 1))
        X_3 = np.random.RandomState(self.seed + 4).uniform(30, 300, (self.N_train, 1))
        y_1 = self.A * X_1.reshape(-1) + self.B + 50 * np.random.RandomState(self.seed + 2).randn(self.N_train)
        y_2 = self.A * X_2.reshape(-1) + self.B + 50 * np.random.RandomState(self.seed + 3).randn(self.N_train)
        y_3 = self.A * X_3.reshape(-1) + self.B + 50 * np.random.RandomState(self.seed + 4).randn(self.N_train)
        return [X_1, X_2, X_3], [y_1, y_2, y_3]
