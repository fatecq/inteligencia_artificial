from ImovelDataset import DataSet
from sklearn.linear_model import LinearRegression
from sklearn.tree import DecisionTreeRegressor
import pandas as pd
import numpy as np

dados = DataSet(seed=2303)

casas = dados.train()
X = casas[0]
y = casas[1]

modelo_RL = LinearRegression()
modelo_RL.fit(X, y)

modelo_DTR = DecisionTreeRegressor(max_depth=2)
modelo_DTR.fit(X, y)

modelo_DTR1 = DecisionTreeRegressor()
modelo_DTR1.fit(X, y)

modelo_DTR2 = DecisionTreeRegressor(min_samples_leaf=3)
modelo_DTR2.fit(X, y)

modelo_DTR3 = DecisionTreeRegressor(min_samples_split=5)
modelo_DTR3.fit(X, y)

casas_valid = dados.valid()
X_valid = casas_valid[0]
y_valid = casas_valid[1]

score_valid = []
score_train = []

# modelo 1 (estudante) de regressão linear (FATEC)
score_valid.append(modelo_RL.score(X_valid, y_valid))
score_train.append(modelo_RL.score(X, y))

# modelo 2 (estudante) de arvore de decisão de 4 folhas (Funvic)
score_valid.append(modelo_DTR.score(X_valid, y_valid))
score_train.append(modelo_DTR.score(X, y))

# modelo 3 (estudante) de arvore de decisão de 20 folhas (Funvic)
score_valid.append(modelo_DTR1.score(X_valid, y_valid))
score_train.append(modelo_DTR1.score(X, y))

# modelo 4
score_valid.append(modelo_DTR2.score(X_valid, y_valid))
score_train.append(modelo_DTR2.score(X, y))

# modelo 5
score_valid.append(modelo_DTR3.score(X_valid, y_valid))
score_train.append(modelo_DTR3.score(X, y))

df_score = pd.DataFrame({'train': score_train, 'valid': score_valid})

"""
#### Reduzindo a sensibilidade do modelo aos dados de treinamento #####
# Primeira alternativa

## Aumentando a quantidade de dados de treinamento
Para que o estudante não dependa da sorte em relação ao conteudo estudado para ir bem no simulado, recomenda-se que ele 
estude toda a matéria que cairá no simulado. Desta forma o estudante estará melhor preparado para a avaliação.

Ocorre o mesmo para os algoritmos de ML. "Estudar toda a matéria" significa ter mais dados de treinamento para o 
algoritmo aprender a tarefa. Quanto mais dados for possível o algoritmo ter para seu treinamento, mais acertivo será 
o seu aprendizado.

Anteriormente, havíamos usado poucos dados de treinamento e isso se assemelha a situação do estudante que não estudou 
toda a matéria para o simulado. Agora vamos verificar que ao usar mais dados de treinamento os modelos de AD serão 
capazes de melhorar suas predições sobre os dados de validação.

repita todos os passos da seção anterior, agora usando o método train_large para que ele gere dados de treinamento com 
500 exemplos (500 casas com a metragem e respectivo preço)

Você deverá notar que os modelos 
"""

# usando o método train_large
X_trainL, y_trainL = dados.train_large()

# fase de treinamento
modelo_RL.fit(X_trainL, y_trainL)
modelo_DTR.fit(X_trainL, y_trainL)
modelo_DTR1.fit(X_trainL, y_trainL)
modelo_DTR2.fit(X_trainL, y_trainL)
modelo_DTR3.fit(X_trainL, y_trainL)

score_trainL = []
score_validL = []

# performance dos modelos (no treinamento e na validação)
score_trainL.append(modelo_RL.score(X_trainL, y_trainL))
score_trainL.append(modelo_DTR.score(X_trainL, y_trainL))
score_trainL.append(modelo_DTR1.score(X_trainL, y_trainL))
score_trainL.append(modelo_DTR2.score(X_trainL, y_trainL))
score_trainL.append(modelo_DTR3.score(X_trainL, y_trainL))

score_validL.append(modelo_RL.score(X_valid, y_valid))
score_validL.append(modelo_DTR.score(X_valid, y_valid))
score_validL.append(modelo_DTR1.score(X_valid, y_valid))
score_validL.append(modelo_DTR2.score(X_valid, y_valid))
score_validL.append(modelo_DTR3.score(X_valid, y_valid))

df_score['trainL'] = score_trainL
df_score['validL'] = score_validL

print(df_score)

"""
# Segunda alternativa

## Reduzindo a sensibilidade do modelo sem aumentar a quantidade de dados de treinamento

Nem sempre será possível ter uma grande quantidade de dados para treinar o seu modelo de ML. Neste caso para contornar 
este problema usamos estratégias que permitem treinar várias vezes o mesmo modelo mudando um pouco os dados de 
treinamento a cada vez que o modelo for treinado.

As principais estratégias sao o Cross-Validation e o Bootstrap.

## Cross Validation

fazer analogia com um estudante (modelo A) que tem rendimento menor mas constante (std baixo) e um estudante (modelo B) 
que tem redimento alto mas irregular (std alto)... O estudante mais confiável é o modelo A !!!

importe cross_val_score do model_selection do sklearn 
"""

from sklearn.model_selection import cross_val_score

# junte os 20 dados de treinamento e 5 de validação. Faça um único dataset com eles e use o cross_val_score para
# olhar as varias performances do modelo.

X_cv = np.concatenate((X, X_valid))
y_cv = np.concatenate((y, y_valid))

notas = []

# fazendo a validação cruzada "cross validation"
notas.append(cross_val_score(modelo_RL, X_cv, y_cv, cv=5))
print(f'\nRL: {notas[0]} - Média: {notas[0].mean()} - Desvio: {notas[0].std()}\n')

notas.append(cross_val_score(modelo_DTR, X_cv, y_cv, cv=5))
print(f'DTR: {notas[1]} - Média: {notas[1].mean()} - Desvio: {notas[1].std()}\n')

notas.append(cross_val_score(modelo_DTR1, X_cv, y_cv, cv=5))
print(f'DTR1: {notas[2]} - Média: {notas[2].mean()} - Desvio: {notas[2].std()}\n')

notas.append(cross_val_score(modelo_DTR2, X_cv, y_cv, cv=5))
print(f'DTR2: {notas[3]} - Média: {notas[3].mean()} - Desvio: {notas[3].std()}\n')

notas.append(cross_val_score(modelo_DTR3, X_cv, y_cv, cv=5))
print(f'DTR3: {notas[4]} - Média: {notas[4].mean()} - Desvio: {notas[4].std()}\n')
